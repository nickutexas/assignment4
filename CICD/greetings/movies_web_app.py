import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

@app.route('/search_movie', methods=['POST'])
def search_movie():
    msg = 'results at bottom of page'

    db, username, password, hostname = get_db_creds()

    inActor = request.form['search_actor']

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, actor FROM movies WHERE actor = '" + inActor + "'")
    entries = [dict(title=row[0], year=row[1], actor=row[2]) for row in cur.fetchall()]
    if len(entries) == 0:
        msg = 'No movies found for actor ' + inActor
    return render_template('index.html', entries=entries, message=msg)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, director, actor, rating FROM movies")
    entries = [dict(title=row[0], year=row[1], actor=row[2], director=row[3], rating=row[4]) for row in cur.fetchall()]
    high = 0.0
    sols = []
    for ent in entries:
        if ent['rating'] > high:
            high = ent['rating']
    for ent in entries:
        if ent['rating'] == high:
            sols.append(ent)
    for ent in sols:
        ent['rating'] = str(ent['rating'])
    return render_template('index.html', entries2=sols, message='results at bottom of page')

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title, year, actor, director, rating FROM movies")
    entries = [dict(title=row[0], year=row[1], actor=row[2], director=row[3], rating=row[4]) for row in cur.fetchall()]
    low = 100.0
    sols = []
    for ent in entries:
        if ent['rating'] < low:
            low = ent['rating']
    for ent in entries:
        if ent['rating'] == low:
            sols.append(ent)
    for ent in sols:
        ent['rating'] = str(ent['rating'])
    return render_template('index.html', entries2=sols, message='results at bottom of page')

@app.route('/delete_movie', methods=['POST'])
def delete_movie():

    db, username, password, hostname = get_db_creds()

    inTitle = request.form['delete_title']
    msg = 'Movie ' + inTitle + ' successfully deleted'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("SELECT title FROM movies ")
        entries = [dict(title=row[0]) for row in cur.fetchall()]
        count = 0
        for entry in entries:
            if entry['title'] == inTitle:
                count = count+1
        if count > 0:
            cmg = "DELETE FROM movies WHERE title='" + inTitle + "';"
            cur.execute(cmg)
            cnx.commit()
        else:
            msg = 'Movie with title ' + inTitle + ' does not exist'
    except Exception as exp:
        msg = 'Movie ' + inTitle + ' could not be deleted - ' + str(exp)
    
    return render_template('index.html', message=msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    inYear = request.form['year']
    inTitle = request.form['title']
    inDirector = request.form['director']
    inActor = request.form['actor']
    inRelease = request.form['release_date']
    inRating = request.form['rating']
    msg = 'Movie ' + inTitle + ' successfully inserted'

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    try:
        cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES ('" + inYear + "', '" + inTitle + "', '" + inDirector + "', '" + inActor + "', '" + inRelease + "', " + inRating + ")")
        cnx.commit()
    except Exception as exp:
        msg = 'Movie ' + inTitle + ' could not be inserted - ' + str(exp)

    return render_template('index.html', message=msg)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    inYear = request.form['year']
    inTitle = request.form['title']
    inDirector = request.form['director']
    inActor = request.form['actor']
    inRelease = request.form['release_date']
    inRating = request.form['rating']
    msg = 'Movie ' + inTitle + ' successfully updated'

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute("SELECT title FROM movies ")
        entries = [dict(title=row[0]) for row in cur.fetchall()]
        count = 0
        for entry in entries:
            if entry['title'] == inTitle:
                count = count+1
        if count > 0:
            cur.execute("UPDATE movies SET year = '" + inYear + "', director = '" + inDirector + "', actor = '" + inActor + "', release_date = '" + inRelease + "', rating = " + inRating + " WHERE title = '" + inTitle + "';")
        else:
            msg = 'Movie with title ' + inTitle + ' does not exist'

        cnx.commit()
    except Exception as exp:
        msg = 'Movie ' + inTitle + ' could not be updated - ' + str(exp)

    return render_template('index.html', message=msg)

@app.route("/")
def hello():
    return render_template('index.html')

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
